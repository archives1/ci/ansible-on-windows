# Ansible on windows

## Install Dependentcy

****This step support Windows only**

Things to prepare before testing

| Tools | Version | Description |
| --- | --- | --- |
| python3 | [3.9.10](https://www.python.org/downloads/release/python-3910/) |  |
| Cygwin | [stable-5.8.0](https://phoenixnap.com/kb/install-ansible-on-windows) | https://phoenixnap.com/kb/install-ansible-on-windows |
| ansible 5 | [stable-5.8.0](https://github.com/ansible/ansible/archive/stable-2.9.tar.gz) |  |


#### Install All dependentcy

```sh

pip3 install -r requirements.txt

```
